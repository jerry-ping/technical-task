import { extendTheme } from "@chakra-ui/react";

const customTheme = extendTheme({
  styles: {
    global: {
      boxSizing: "border-box"
    },
  },
});

export default customTheme;
