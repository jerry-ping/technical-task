import React, { useCallback, useState } from "react";
import { useHistory } from "react-router";
import {
  Stack,
  Heading, 
  Button, 
} from "@chakra-ui/react";
import InputField from "../components/InputField";

// type ApplicationType = {
//   fullname: string;
//   email: string;
//   phone: string;
//   amount: number;
//   revenue: number;
// }

type FieldDataType = {
  value: string;
  hasError: boolean;
}

const ApplicationFormFields = [
  { property: "fullname", label: "Full Name", type: "string", required: true },
  { property: "email", label: "Email", type: "email", required: true},
  { property: "phone", label: "Phone Number", type: "phone", required: false},
  { property: "amount", label: "Founding amount requested", type: "number",  required: false},
  { property: "revenue", label: "Yearly revenue", type: "number", required: true}
]

function Application() {
  const history = useHistory();
  const [fieldData, setFieldData] = useState<FieldDataType[]>(ApplicationFormFields.map((_field) => ({ value: "", hasError: _field.required ? true : false })));

  const handleChange = useCallback((changedField: any, id: number) => {
    setFieldData((_fieldData) => {
      return _fieldData.map((_field: FieldDataType, _id: number) => id === _id ? changedField : _field)
    });
  }, []);

  const handleApply = useCallback(() => {
    if (Number(fieldData[4].value) >= 50000) {
      history.push("/thankyou");
    } else {
      history.push("no-offer")
    }
  }, [fieldData, history]);

  return (
    <>
      <Heading textAlign="center">Application</Heading>
      <Stack spacing={4} mt="10">
        {ApplicationFormFields.map((_field: any, id) =>(
          <InputField
            key={_field.property}
            label={_field.label}
            property={_field.property} 
            value={fieldData[id].value} 
            handleChange={handleChange}
            type={_field.type}
            fieldId={id}
            required={_field.required}
          />
        ))}
      </Stack>
      <Button isDisabled={fieldData.findIndex((_field: FieldDataType) => _field.hasError === true) !== -1} onClick={handleApply} w="full" size="lg" colorScheme="green" mt={10}>
        Apply
      </Button>
    </>
  );
}

export default Application;
