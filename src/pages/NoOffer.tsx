import React from "react";
import { useHistory } from "react-router";
import { Heading, Button } from "@chakra-ui/react";

function NoOffer() {
  const history = useHistory();
  return (
    <>
      <Heading textAlign="center">No Offer</Heading>
      <Button mt="300px" onClick={() => history.push("/")} w="full" size="lg" colorScheme="green">
        GO TO APPLICATION PAGE
      </Button>   
    </>
  );
}

export default NoOffer;
