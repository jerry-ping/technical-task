type FieldType = "string" | "email" | "phone" | "number"

export const validateField = (input: string, inputType: FieldType) => {
  if (inputType === "string") return true;

  const conditions = {
    //eslint-disable-next-line
    phone: /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im,
    //eslint-disable-next-line
    email: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    //eslint-disable-next-line
    number: /^\d*$/
  };
  
  return conditions[inputType].test(input);
}