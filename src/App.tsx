import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import { 
  ChakraProvider,
  Box,
  Flex
} from "@chakra-ui/react";
import "./App.css";
import Application from "./pages/Application";
import NoOffer from "./pages/NoOffer";
import Thankyou from "./pages/Thankyou";
import customTheme from "./styles/customTheme";

function App() {
  return (
    <ChakraProvider theme={customTheme}>
      <Flex w="full" minH="100vh" p="30px" bg="#E4E5E5" justifyContent="center" alignItems="center">
        <Box w="full" maxW={{ "base": "100%", "md": "500px" }} minH="500px" p={{ base: "20px", md: "40px" }} borderTop="5px solid #00AEBB" bg="white">
          <Router>
            <Switch>
              <Route exact path="/thankyou">
                <Thankyou />
              </Route>
              <Route exact path="/no-offer">
                <NoOffer />
              </Route>
              <Route exact path="/">
                <Application />
              </Route>
              <Route exact path="*">
                <Redirect to="/" />
              </Route>
            </Switch>
          </Router>
        </Box>
      </Flex>
    </ChakraProvider>
  );
}

export default App;
