import React, { useCallback, useState } from "react";
import { 
  Input,
  Text,
  Box
} from "@chakra-ui/react";
import { validateField } from "../utils/validation";

type InputFieldProps = {
  fieldId: number;
  property: string;
  value: string;
  label: string;
  handleChange: (fieldData: any, id: number) => void;
  required: boolean;
  type: "string" | "email" | "phone" | "number"
}

const InputField = ({ fieldId, property, value, label, handleChange, required, type }: InputFieldProps) => {
  const [error, setError] = useState("");

  const validate = useCallback((event) => {
    if (!required && event.target.value === "") {
      return "";
    }

    if (required && event.target.value === "") {
      return "Required";
    } 

    if (!validateField(event.target.value, type)) {
      return `Incorrect ${type}!`;
    }

    return "";
  }, [required, type]);

  const handleInputChange = useCallback((event) => {
    const errorText = validate(event);

    if (error !== "" && errorText === "") {
      setError("");
    }

    handleChange({ value: event.target.value, hasError: errorText === "" ? false : true }, fieldId);
  }, [validate, error, handleChange, fieldId]);

  const handleInputBlur = useCallback((event) => {
    setError(validate(event))
  }, [validate]);

  return (
    <Box>
      <Text fontSize="sm">{`${label}${required ? " *" : ""}`}</Text>
      <Input 
        mt="2"
        placeholder={label}
        size="md" 
        value={value}
        onChange={handleInputChange}
        onBlur={handleInputBlur}
      />
      {error && <Text color="red" mt="1" fontSize="xs">{error}</Text>}
    </Box>
  );
}

InputField.defaultProps = {
  required: false,
  type: "string"
};

export default React.memo(InputField);
